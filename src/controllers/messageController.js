const Joi = require('joi');
const model = require('../databases/models');
const {scheduler, logger} = require('../helpers');
const { Op } = require('sequelize');
const { ValidationError } = require('joi');

module.exports = {
    store: async (req, res) => {
        const schema = Joi.object({
            title: Joi.string().max(100).required(),
            message: Joi.string().max(160).required(),
            time: Joi.date().required(),
            recipients: Joi.array().items(Joi.string().regex(/^\d+$/)).required()
        });
        const body = req.body;
        // First, we start a transaction and save it into a variable
        const t = await model.sequelize.transaction();
        try {
            const value = await schema.validateAsync(body);

            const campaign = await model['campaigns'].create({
                title: value.title,
                body: value.message,
                run: value.time
            }, {transaction: t});

            const dataRecipient = [];

            for (let i=0, iLen=value.recipients.length;i < iLen;i++) {
                const recipient = {
                    phone: value.recipients[i],
                    campaignId: campaign.id
                };
                dataRecipient.push(recipient);
            }
            
            await model['recipients'].bulkCreate(dataRecipient, {transaction: t});
            await scheduler.scheduleMessage(campaign.id, value.time, value.recipients, value.message);
            await t.commit();
            return res.jsonSuccess({campaign});
        }
        catch (err) { 
            await t.rollback();
            const resErr = {
                error: err.message
            };
            if (err instanceof ValidationError) {
                resErr.code = 400;
                resErr.message = 'Validation Error';
            }
            logger.error('error found when creating a campaign: '+err.message);
            return res.jsonFail(resErr);
        }
    },

    schedule: async (req, res) => {
        try {
            const schema = Joi.object({
                page: Joi.number().integer().min(1).default(1),
                perPage: Joi.number().integer().min(1).default(5),
                start: Joi.date().default('2000-01-01T00:00:00Z'),
                end: Joi.date().min(Joi.ref('start'))
            });
            const value = await schema.validateAsync(req.query);

            const {page, perPage, start, end} = value;
            const limit = perPage;
            const offset = limit * (page - 1);
            const where = {};
            const condition = {};

            if (start !== undefined) {
                condition.gte = {
                    [Op.gte]: start
                };
            }
            
            if (end !== undefined) {
                condition.lte = {
                    [Op.lte]: end
                };
            }
            
            if (start !== undefined || end !== undefined) {
                where.run = {...condition.gte, ...condition.lte};
            }

            const schedule = await model['campaigns'].findAndCountAll({
                limit,
                offset,
                where
            });

            const data = schedule.rows;
            const totalPage = Math.ceil(schedule.count/limit);
            
            return res.jsonSuccess({data, totalPage, totalItems: schedule.count, currentPage: page});
            
        } catch (err) {
            const resErr = {
                error: err.message
            };
            if (err instanceof ValidationError) {
                resErr.code = 400;
                resErr.message = 'Validation Error';
            }
            logger.error('error found when get list schedule: '+err.message);
            return res.jsonFail(resErr);
        }
    },

    listSms: async (req, res) => {
        try {
            const schema = Joi.object({
                page: Joi.number().integer().min(1).default(1),
                perPage: Joi.number().integer().min(1).default(5),
                status: Joi.string().valid('ACCEPTD', 'DELIVRD', 'UNDELIV', 'UNKNOWN')
            });
            const value = await schema.validateAsync(req.query);

            const {page, perPage, status} = value;
            const limit = perPage;
            const offset = limit * (page - 1);
            const where = {
                campaignId: req.params.id
            };

            if (status !== undefined) {
                where.status = status;
            }

            const recipient = await model['recipients'].findAndCountAll({
                limit,
                offset,
                where
            });

            const data = recipient.rows;
            const totalPage = Math.ceil(recipient.count/limit);
            
            return res.jsonSuccess({data, totalPage, totalItems: recipient.count, currentPage: page});
        } catch (err) {
            const resErr = {
                error: err.message
            };
            if (err instanceof ValidationError) {
                resErr.code = 400;
                resErr.message = 'Validation Error';
            }
            logger.error('error found when get list sms: '+err.message);
            return res.jsonFail(resErr);
        }
    }
};