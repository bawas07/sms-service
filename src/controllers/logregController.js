const Joi = require('joi');
const jwt = require('jsonwebtoken');
const config = require('config');

const {logger} = require('../helpers');
const model = require('../databases/models');

module.exports = {
    register: async (req, res) => {
        try {
            const schema = Joi.object({
                username: Joi.string().required(),
                password: Joi.string().required(),
            });
            const value = await schema.validateAsync(req.body);
            const user = await model['users'].create(value);
            return res.jsonSuccess({data: user});
        } catch (err) {
            const resErr = {
                error: err.message
            };
            if (err instanceof Joi.ValidationError) {
                resErr.code = 400;
                resErr.message = 'Validation Error';
            }
            logger.error('error found when register new user: '+err.message);
            return res.jsonFail(resErr);
        }
    },
    login: async (req, res) => {
        try {
            const schema = Joi.object({
                username: Joi.string().required(),
                password: Joi.string().required(),
            });
            const value = await schema.validateAsync(req.body);
            const user = await model['users'].findOne({
                where: {
                    username: value.username
                }
            });
            if (user == null) {
                logger.info('Login failed detected');
                const resBody = {
                    code: 401,
                    message: 'Login Failed',
                    error: 'username or password is incorrect'
                };
                return res.jsonFail(resBody);
            }

            // check password
            const valid = user.validPassword(value.password);
            if (!valid) {
                logger.info('Login failed detected');
                const resBody = {
                    code: 401,
                    message: 'Login Failed',
                    error: 'username or password is incorrect'
                };
                return res.jsonFail(resBody);
            }
            
            const dataToken = {
                username: value.username
            };

            const dataRefresh = {
                id: user.id
            };
            
            const jwtOpts = config.get('jwt_opts');
            const refreshOpts = config.get('refresh_opts');
            const secret = config.get('jwt_secret');
            const refreshSecret = config.get('refresh_secret');

            // generate token
            const token = jwt.sign(dataToken, secret, jwtOpts);
            const refreshToken = jwt.sign(dataRefresh, refreshSecret, refreshOpts);

            user.token = refreshToken;
            await user.save();

            return res.jsonSuccess({token, refreshToken});
        } catch (err) {
            const resErr = {
                error: err.message
            };
            if (err instanceof Joi.ValidationError) {
                resErr.code = 400;
                resErr.message = 'Validation Error';
            }
            logger.error('error found login: '+err.message);
            return res.jsonFail(resErr);
        }
    },
    token: async (req, res) => {
        try {
            if (!req.headers.authorization) {
                const resBody = {
                    code: 401,
                    message: 'Unauthorized',
                    error: 'Login required'
                };
                return res.jsonFail(resBody);
            }
    
            const auth = req.headers.authorization;
            const token = auth.split(' ')[1];
            const secret = config.get('jwt_secret');
            const refreshSecret = config.get('refresh_secret');

            // check refresh token validity
            jwt.verify(req.body.refresh, refreshSecret);

            // check access token payload
            const decoded = jwt.verify(token, secret, {ignoreExpiration: true});

            const user = await model['users'].findOne({
                where: {
                    username: decoded.username,
                    token: req.body.refresh
                }
            });

            if (user == null) {
                throw new Error ('Invalid Token');
            }
            const dataToken = {
                username: user.username
            };

            const dataRefresh = {
                id: user.id
            };
            const jwtOpts = config.get('jwt_opts');
            const refreshOpts = config.get('refresh_opts');

            // generate new token
            const newToken = jwt.sign(dataToken, secret, jwtOpts);
            const refreshToken = jwt.sign(dataRefresh, refreshSecret, refreshOpts);

            user.token = refreshToken;
            await user.save();

            return res.jsonSuccess({token: newToken, refreshToken});
        } catch (err) {
            logger.error('error found when refreshing token: '+err.message);
            return res.jsonFail({error: err.message});
        }
    }
};