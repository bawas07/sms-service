const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = {
    checkToken: (req, res, next) => {
        try {
            if (!req.headers.authorization) {
                const resBody = {
                    code: 401,
                    message: 'Unauthorized',
                    error: 'Login required'
                };
                return res.jsonFail(resBody);
            }
    
            const auth = req.headers.authorization;
            const token = auth.split(' ')[1];
            const secret = config.get('jwt_secret');
    
            const decoded = jwt.verify(token, secret);
            req.userData = decoded;
            next();

        } catch (err) {
            const resErr = {};
            resErr.code = 401;
            resErr.message = 'Unauthorized';
            resErr.error = 'login required';
                
            return res.jsonFail(resErr);
        }
    }
};