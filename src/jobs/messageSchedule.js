const Queue = require('bull');
// const logger = require('../helpers/logger');
const redisConnection = require('../helpers/redisConnection');
// const message = require('../helpers/message');
// const scheduler = require('../helpers/scheduler');
const model = require('../databases/models');
const _ = require('lodash');
// const helper = require('../helpers');
const opts = redisConnection;

const messageSchedule = new Queue('message-schedule', opts);
messageSchedule.process(async (job) => {
    const {scheduler,message, logger} = require('../helpers');
    try {
        const data = job.data;
        const rawRecipients = await model['recipients'].findAll({
            where: {
                campaignId: data.id
            },
            attributes: ['phone']
        });
        const recipients = [];
        for (let i=0;i< rawRecipients.length;i++) {
            recipients.push(rawRecipients[i].phone);
        }
        logger.info('send campaign with id: '+data.id);
        const resp = await message.send(recipients, data.message);
        // save message_id to database
        const queries = [];
        for (let i = 0; i<resp.length;i++) {
            const whereOpts = {
                phone: resp[i].dnis,
                campaignId: data.id
            };
            const opts = {
                messageId: resp[i].message_id
            };
            queries.push(model['recipients'].update(opts, {where: whereOpts}));
        }

        // Create batch update
        const batches = _.chunk(queries, 100);
        while (batches.length) {
            const batch = batches.shift();
            await Promise.all(batch);
        }
        setTimeout(function(){
            // update data that just sent
            scheduler.updateMessage(data.id);
        }, 300000);
        return Promise.resolve({ campaign_id:data.id, finishedOn: new Date() });
    } catch (error) {
        Promise.reject(error);
    }
});

module.exports = messageSchedule;