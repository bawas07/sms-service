'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class recipients extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            // define association here
        }
    }
    recipients.init({
        phone: DataTypes.STRING,
        campaignId: DataTypes.STRING,
        messageId: DataTypes.STRING,
        status: DataTypes.STRING
    }, {
        sequelize,
        paranoid: true,
        modelName: 'recipients',
    });
    
    recipients.associate = function(models) {
        recipients.belongsTo(models.campaigns);
    };
    return recipients;
};