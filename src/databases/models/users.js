'use strict';
const {
    Model
} = require('sequelize');

const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
    class users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            // define association here
        }
    }
    users.init({
        username: DataTypes.STRING,
        password: DataTypes.STRING,
        token: DataTypes.STRING
    }, {
        sequelize,
        paranoid: true,
        modelName: 'users',
    });

    users.addHook('beforeCreate', async (user, options) => {
        const salt = await bcrypt.genSalt();
        user.password = await bcrypt.hash(user.password, salt);
    });

    users.prototype.validPassword = function(password) {
        return bcrypt.compareSync(password, this.password);
    };

    users.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
      
        delete values.password;
        delete values.token;
        return values;
    };
    return users;
};