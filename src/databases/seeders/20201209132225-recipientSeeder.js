'use strict';

const faker = require('faker');

module.exports = {
    up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
        const status = ['ACCEPTD', 'DELIVRD', 'UNDELIV', 'UNKNOWN'];
        const insert = [];
        for (let i=0;i<10;i++) {
            const query = {
                phone: faker.phone.phoneNumber('############'),
                campaignId: Math.floor((Math.random() * 5) + 1),
                messageId: faker.random.uuid(),
                status: status[Math.floor(Math.random() * 4)],
                createdAt: faker.date.past(),
                updatedAt: faker.date.past()
            };
            insert.push(query);
        }
        console.log('Seeding 10 row to table Campaign');
   
        await queryInterface.bulkInsert('recipients', insert, {});
    },

    down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    }
};
