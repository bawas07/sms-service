'use strict';
const faker = require('faker');

module.exports = {
    up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
        const insert = [];
        for (let i=0;i<5;i++) {
            const query = {
                title: faker.lorem.word(),
                body: faker.lorem.sentence(),
                run: faker.date.soon(),
                createdAt: faker.date.past(),
                updatedAt: faker.date.past()
            };
            insert.push(query);
        }
        console.log('Seeding 5 row to table Campaign');
        
        await queryInterface.bulkInsert('campaigns', insert, {});
    },

    down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    }
};
