'use strict';
const bcrypt = require('bcrypt');

module.exports = {
    up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
        const insert = [];
        const salt = await bcrypt.genSalt();
        const password = await bcrypt.hash('pass', salt);
        const user = {
            username: 'user',
            password: password,
            createdAt: new Date(),
            updatedAt: new Date()
        };
        const user2 = {
            username: 'user2',
            password: password,
            createdAt: new Date(),
            updatedAt: new Date()
        };
        insert.push(user);
        insert.push(user2);
        
        await queryInterface.bulkInsert('users', insert, {});
    },

    down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    }
};
