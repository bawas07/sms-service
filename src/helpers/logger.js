const winston = require('winston');
const config = require('config');
const loggerPath = config.get('log_path');

const opts = {
    file: {
        level: 'info',
        filename: loggerPath,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    console: {
        format: winston.format.combine(
            winston.format.prettyPrint(),
            winston.format.timestamp(),
            winston.format.printf(i => `${i.timestamp} | ${JSON.stringify(i.message, null, 2)}`)
        ),
        level: 'debug',
        handleExceptions: true,
    },
};

const logger = winston.createLogger({
    transports: [
        new winston.transports.File(opts.file),
        new winston.transports.Console(opts.console)
    ],
    exitOnError: false, // do not exit on handled exceptions
});

logger.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};

module.exports = logger;