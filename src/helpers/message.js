const axios = require('axios');
const config = require('config');
const logger = require('./logger');

module.exports = {
    /**
     * Send a messages
     * 
     * @param {Array<string>} recipients phone number
     * @param {string} message message body
     */
    send: async (recipients, message) => {
        logger.info(new Date() + 'sending message with body: '+message);
        const strRecipients = recipients.join(',');
        try {
            const sent = await axios.post(config.get('sms_send_url'), {
                dnis: strRecipients,
                message: message
            });
            if (recipients.length == 1) {
                const result = [{
                    dnis: recipients[0],
                    message_id: sent.data.message_id
                }];
                return result;
            }
            return sent.data;
        } catch (error) {
            logger.error('error when sending message: '+ error.message);
            return error.message;
        }
    },

    /**
     * Get Message Status
     * 
     * @param {string} id message id
     */
    get: async (id) => {
        try {
            const messageStatus = await axios.get(config.get('sms_status_url')+'?messageId='+id);
            return messageStatus.data;
        } catch (error) {
            return error.message;
        }
    }
};