const Redis = require('ioredis');
const config = require('config');

const PORT = config.get('redis.port');
const HOST = config.get('redis.host');
const client = new Redis(PORT, HOST);
const subscriber = new Redis(PORT, HOST);

const opts = {
    createClient(type) {
        switch (type) {
        case 'client':
            return client;
        case 'subscriber':
            return subscriber;
        default:
            return new Redis(PORT, HOST);
        }
    },
};

module.exports = opts;