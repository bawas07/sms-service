const { Op } = require('sequelize');
const _ = require('lodash');
const model = require('../databases/models');
const logger = require('./logger');
const message = require('./message');
const job = require('../jobs');


module.exports = {
    /**
     * Send a scheduled messages
     * 
     * @param {Number} id campaign's id
     * @param {Date} time A time to run at
     * @param {Array<string>} recipients phone number
     * @param {string} message message body
     */
    scheduleMessage: (id, time, recipients, body) => {
        logger.info('create a job to run at: '+time);

        // calculate delay in ms
        const sentTime = time;
        const currentTime = new Date();
        const delay = (sentTime.getTime() - currentTime.getTime());

        job.add('messageSchedule', {id: id, message: body}, {delay});
        return;
    },

    /**
     * Update Message Status
     */
    updateMessage: async (id = null) => {
        const where = {
            status: {
                [Op.or]: [null, 'ACCEPTD']
            },
            messageId: {
                [Op.ne]: null
            }
        };

        if (id !== null) {
            where.campaignId = id;
        }
        
        const recipients = await model['recipients'].findAll({
            where: where
        });

        const queries = [];
        for (let i = 0; i<recipients.length;i++) {
            const query = async () => {
                const item = recipients[i];
                const id = item.messageId;
                const dataMessage = await message.get(id);
                if (item.status !== dataMessage.status) {
                    item.status = dataMessage.status;
                    await item.save();
                }
                return;
            };
            queries.push(query);
        }

        // Create batch update
        const batches = _.chunk(queries, 50);
        while (batches.length) {
            const batch = batches.shift();
            await Promise.all(batch.map(fun => fun()));
        }
        return;
    }
};