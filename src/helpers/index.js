const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const helper = {};

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const location = require(path.join(__dirname, file));
        const name = file.split('.js')[0];
        helper[name] = location;
    });

module.exports = helper;