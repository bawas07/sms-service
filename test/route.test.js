const request = require('supertest');
const app = require('../app.js');
const model = require('../src/databases/models');

jest.mock('../src/helpers/scheduler');
jest.mock('../src/helpers/logger');

const scheduler = require('../src/helpers/scheduler');
const logger = require('../src/helpers/logger');
const c = require('config');

scheduler.scheduleMessage.mockResolvedValue(null);
logger.info.mockResolvedValue(null);

let token;

beforeAll(async (done) => {
    try {
        const dataLogin = {
            username: 'user2',
            password: 'pass',
        };
        // get token for authorization
        const login = await request(app)
            .post('/api/login')
            .send(dataLogin);
        
        token = login.body.payload.token;
    } catch (err) {
        console.log({err});
        
    }
});

describe('Base API', () => {
    it('should show ping', async () => {
        const res = await request(app).get('/api');
        expect(res.statusCode).toEqual(200);
        expect(res.body.payload).toHaveProperty('ping');
    });
});

describe('Get List Campaign', () => {
    it('should show all campaign', async () => {
        const res = await request(app)
            .get('/api/schedule')
            .set('Authorization', `Bearer ${token}`);
        expect(res.statusCode).toEqual(200);
        expect(res.body.payload).toHaveProperty('data');
    });
});

describe('Get List SMS', () => {
    it('should show all campaign', async () => {
        const res = await request(app)
            .get('/api/schedule/1')
            .set('Authorization', `Bearer ${token}`);
        expect(res.statusCode).toEqual(200);
        expect(res.body.payload).toHaveProperty('data');
    });
});

describe('Create SMS', () => {
    it('should Create campaign', async () => {
        const data = {
            title: 'test campaign',
            message: 'aha',
            time: '2020-12-09T10:08:10Z',
            recipients: [
                '621111111',
                '622222222222'
            ]
        };
        const res = await request(app)
            .post('/api/campaign')
            .set('Authorization', `Bearer ${token}`)
            .send(data);
        
        // Check response
        expect(res.statusCode).toEqual(200);
        expect(res.body.payload).toHaveProperty('campaign');
        
        // Check data in table campaign
        const id = res.body.payload.campaign.id;
        const campaign = await model['campaigns'].findOne({ where: {id} });
        expect(campaign.title).toEqual(data.title);
        expect(campaign.run).toEqual(new Date(data.time));
        expect(campaign.body).toEqual(data.message);

        const recipients = await model['recipients'].findAll({where: {campaignId: id}});
        expect(recipients.length).toEqual(data.recipients.length);
    });
});