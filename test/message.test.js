const message = require('../src/helpers/message');
const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const mock = new MockAdapter(axios);

jest.mock('../src/helpers/logger');
const logger = require('../src/helpers/logger');
logger.info.mockResolvedValue(null);

describe('Message Test', () => {
    it('should send the message', async () => {
        const sent = {
            message_id: '123123'
        };
        mock.onPost('http://kr8tif.lawaapp.com:1338/api').reply(200, sent);
        const send = await message.send(['621111111111'], 'test message');
        expect(send[0].message_id).toBeDefined();
    });

    it('should get message status', async () => {
        const resp = {
            status: 'UNDELIV',
            delivery_time: '2012060034',
            mccmnc: ''
        };
        mock.onGet('http://kr8tif.lawaapp.com:1338/api?messageId=508bce61-b3ca-46a2-a799-29b5d12a8a60').reply(200, resp);
        const get = await message.get('508bce61-b3ca-46a2-a799-29b5d12a8a60');
        expect(get.status).toBeDefined();
    });
});