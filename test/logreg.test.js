const request = require('supertest');
const app = require('../app.js');
const model = require('../src/databases/models');


describe('Login api', () => {
    it('should return token', async () => {
        const data = {
            username: 'user',
            password: 'pass',
        };
        const res = await request(app)
            .post('/api/login')
            .send(data);
        
        expect(res.statusCode).toEqual(200);
        expect(res.body.payload).toHaveProperty('token');
        expect(res.body.payload).toHaveProperty('refreshToken');
    });
});

describe('Register api', () => {
    it('should save new user', async () => {
        const data = {
            username: 'user3',
            password: 'pass',
        };
        const res = await request(app)
            .post('/api/register')
            .send(data);
        
        expect(res.statusCode).toEqual(200);
        expect(res.body.payload.data.username).toEqual('user3');
        expect(res.body.payload.data).toHaveProperty('id');
        
        const user = await model['users'].findOne({
            where: {
                username: 'user3'
            }
        });

        expect(user.id).toEqual(res.body.payload.data.id);
    });
});

describe('Refresh Token', () => {
    it('should generate new token', async () => {
        const dataLogin = {
            username: 'user',
            password: 'pass',
        };
        const login = await request(app)
            .post('/api/login')
            .send(dataLogin);
        
        const data = {
            refresh: login.body.payload.refreshToken
        };
        const res = await request(app)
            .post('/api/token')
            .set('Authorization', `Bearer ${login.body.payload.token}`)
            .send(data);

        expect(res.statusCode).toEqual(200);
        expect(res.body.payload).toHaveProperty('token');
        expect(res.body.payload).toHaveProperty('refreshToken');
        const user = await model['users'].findOne({username: 'user'});
        expect(user.token).toEqual(res.body.payload.refreshToken);
    });
});