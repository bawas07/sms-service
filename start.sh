if [ "$NODE_ENV" != "PRODUCTION" ]
then
    npx sequelize db:migrate
else
    echo "Not running migration in production environment"
fi
node bin/www