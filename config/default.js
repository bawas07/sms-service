const path = require('path');

module.exports = {
    host: process.env.HOST || '0.0.0.0',
    port: process.env.PORT || 3000,
    sms_send_url: 'http://kr8tif.lawaapp.com:1338/api',
    sms_status_url: 'http://kr8tif.lawaapp.com:1338/api',
    log_path: path.join(__dirname, '/../logs/infos.log'),
    redis: {
        host: process.env.REDIS_HOST || '0.0.0.0',
        port: process.env.REDIS_PORT || 6379,
    },
    jwt_opts: {
        expiresIn: 300 // in second
    },
    refresh_opts: {
        expiresIn: '7d'
    },
    jwt_secret: process.env.JWT_SECRET || 'JWTSECRET',
    refresh_secret: process.env.REFRESH_SECRET || 'REFRESHSECRET'
};