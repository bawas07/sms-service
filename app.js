const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const {logger, redisConnection} = require('./src/helpers');

const indexRouter = require('./routes/index');
const apiRouter = require('./routes/api');
const helmet = require('helmet');
const rateLimit = require('express-rate-limit');
const RedisStore = require('rate-limit-redis');
const app = express();

app.use(helmet());

// define limiter
const apiLimiter = rateLimit({
    store: new RedisStore({
        client: redisConnection.createClient()
    }),
    windowMs: 1 * 60 * 1000, // 1 minutes
    max: 60 // maximum number of requests
});

// define request logger
app.use(morgan(':date[web] - info: :method :status :url :response-time ms', {stream: logger.stream}));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// extend response json
app.use(function(req, res, next) {

    res.jsonFail = function(body) {
        const message = body.message || 'Error found';
        delete body.message;
        const code = body.code || 500;
        delete body.code;
        return res.status(code).json({
            status: false,
            message: message,
            payload: body
        });
    };

    res.jsonSuccess = function(body) {
        const message = body.message || 'Process Success';
        delete body.message;
        return res.json({
            status: true,
            message: message,
            payload: body
        });
    };

    next();
});


app.use('/', indexRouter);
app.use('/api', apiLimiter, apiRouter);

//Capture All 404 errors
app.use(function (req,res,next){
    res.status(404).json({
        status: false,
        message: 'Page not found',
        payload: null
    });
});

module.exports = app;
