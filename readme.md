# SMS Scheduler Service

This is an app that send bulk message at given time

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

you need these software installed on your machine
- docker
- nodejs and npm
- IDE or editor

### Initial Set-up

- Copy or clone this repository
- run `npm install` to install dependencies
- edit .env file to support the env needed
- to run the migration, use command `npx sequelize db:migrate`
- run `node bin/www` to run the program locally

### Using Docker
- to build and run the programs with databases needed run `docker-compose up --build`
- to run the migration type command `docker exec -it message npx sequelize db:migrate`

### Test
- to run the test run command `npm test`

## Folder Structure
```
📦message-service
 ┣ 📂bin
 ┃ ┗ 📜www
 ┣ 📂config
 ┃ ┣ 📜config.js
 ┃ ┗ 📜default.js
 ┣ 📂logs
 ┣ 📂public
 ┣ 📂routes
 ┃ ┣ 📜api.js
 ┃ ┗ 📜index.js
 ┣ 📂src
 ┃ ┣ 📂controllers
 ┃ ┣ 📂databases
 ┃ ┃ ┣ 📂migrations
 ┃ ┃ ┣ 📂models
 ┃ ┃ ┗ 📂seeders
 ┃ ┣ 📂helpers
 ┃ ┗ 📂jobs
 ┣ 📂test
 ┣ 📜.dockerignore
 ┣ 📜.env
 ┣ 📜.eslintrc.js
 ┣ 📜.gitignore
 ┣ 📜.sequelizerc
 ┣ 📜app.js
 ┣ 📜docker-compose.yml
 ┣ 📜dockerfile
 ┣ 📜example.env
 ┣ 📜logs.log
 ┣ 📜package-lock.json
 ┣ 📜package.json
 ┣ 📜readme.md
 ┗ 📜start.sh
```

| File        | Description | 
| :---        |    :----   | 
| `bin/www` | Entripoint for this project    |
| `config/*`   | constant variable that used in this project |
| `logs*` | Location of the app logs |
| `public/*` | Location of public files |
| `routes/*`    | location of the application endpoint |
| `src/controller/*` | The logic location, all the process happens here |
| `src/databases/*` | Contains models, migration, and seeder of the application. This is where database structure is written    |
| `src/helpers/*` | Small function that needed more than once |
| `src/jobs/*` | Queue and scheduled job written in here |

## How to use
- When creating another api endpoint group, make sure you add the routes/api.js

## Deployment

If you deploy build using docker. you can use build the image using dockerfile and push it to Container Registry

## Built With

* [Express JS](https://expressjs.com/) - The web framework used
* [Sequelize](https://sequelize.org/) - Orm used
* [Bull](https://optimalbits.github.io/bull/) - Queue manager
* [Winston](https://github.com/winstonjs/winston) - Logger
* [Axios](https://github.com/axios/axios) - Promise Based HTTP Client
* [Jest](https://jestjs.io/) - Javascript Testing Framework
* [Supertest](https://github.com/visionmedia/supertest) - HTTP Assertions Library
