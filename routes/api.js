const express = require('express');
const router = express.Router();
const controller = require('../src/controllers');
const auth = require('../src/middlewares/auth');

/* GET users listing. */
router.get('/', controller.indexController.index);
router.post('/login', controller.logregController.login);
router.post('/register', controller.logregController.register);
router.post('/token', controller.logregController.token);

// Route with authentification below here
router.use('/', auth.checkToken);

router.post('/campaign', controller.messageController.store);
router.get('/schedule', controller.messageController.schedule);
router.get('/schedule/:id', controller.messageController.listSms);


module.exports = router;
