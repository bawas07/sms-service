FROM node:12

WORKDIR /usr/src/app

COPY . .

RUN npm install --only=prod

# RUN npx sequelize db:migrate

COPY ./example.env ./.env

EXPOSE 8000

# RUN chmod 755 ./start.sh

CMD [ "node", "bin/www" ]
# CMD [ "./start.sh" ]